from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Content(models.Model):
    class Meta():
        db_table = 'content'
    block_name = models.CharField(max_length=40)
    title = models.CharField(max_length=100)
    content = models.CharField(max_length=500)