from django.shortcuts import render_to_response
from models import Content
# Create your views here.

def index(request):
    offer = Content.objects.get(block_name='offer')
    case = Content.objects.get(block_name='case')
    blog = Content.objects.get(block_name='blog')
    we_are = Content.objects.filter(block_name='we_are')
    return render_to_response('index.html', {'case':case, 'offer':offer, 'blog':blog, 'we_are':we_are})