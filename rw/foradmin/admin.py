from django.contrib import admin
from models import Content

# Register your models here.

class ContentAdmin(admin.ModelAdmin):
    list_display = ('block_name', 'title', 'content')

admin.site.register(Content)