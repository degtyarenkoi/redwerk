$(document).ready(function(){
	//Showing&hiding search form
	$("#find").hover(function(){
		$("#search_field").show(1000);
	});
	$("#slider").hover(function(){
		$("#search_field").hide(1000);
	});

	$("#mini-find").hover(function(){
		$("#mini-search_field").show(1000);
	});
	$("#slider").hover(function(){
		$("#mini-search_field").hide(1000);
	});
	//==================================

	//Showing pop up window
	$("#request").click(function(e){
		    $('#popup').fadeIn("slow");
		    $('#hover').fadeIn("fast");
	});
	$("#mini-request").click(function(e){
		    $('#popup').fadeIn("slow");
		    $('#hover').fadeIn("fast");
	});
	$("#exit").click(function(){
		$('#popup').fadeOut("slow");
		$('#hover').fadeOut("fast");
	});
	//==================================

	//Responsive header
	var header = 0;

	$("#mini-menu-button").click(function(){
		if (header == 0) {
			header = 1;
			$("header").animate({height: "350px"}, 1500);
			$("#mini-wrap").slideDown(1000);
		}
		else {
			header = 0;
			$("#mini-wrap").slideUp(1000);
			$("header").animate({height: "100px"}, 800);
		}
	});

	$(window).resize(function(){
		$("#mini-wrap").css({"display": "none"});
		$("header").css({"height": "100px"});

		header = 0;
	});
	//==================================

});